/*========================================================================
 * Author:  S1110307085, Sabine Winkler
 * Group:   MMP5UEG1
 * Since:   2013-10-31
 *
 * The coin count is always in the range [0, 19].
 * If the coin count would become >= 20, the player gets a further life
 * and the coin count is reset to 0 (plus the won coins of the last play,
 * if there are still any left).
 * If the coin count would become negative, the player loses one life and the
 * coin count is reset to 10 (minus the lost coins of the last play,
 * if there are still any left).
 * One run costs 2 coins, which are substracted from the reached result
 * at the end of one run. The total result (result - 2) is shown
 * right beside of the coin count. A run can also be started, if the coin
 * coint is already 0, because the player could still win with a result >= 2.
 * If the life count is 0 and the coins would become negative, the player
 * loses and a "Game Over" alert is printed. The slotmachine is reset
 * to coin count = 10 and life count = 1.
 ======================================================================== */

var Mediator = (function () {

    var channels = {};

    //
    // Subscribe to a channel
    //
    function subscribe(channel, fn, that) {
        if (!channels[channel]) {
            channels[channel] = [];
        }
        channels[channel].push({
            context: that,
            callback: fn
        });
    }

    //
    // Publish a channel (like triggering an event)
    //
    function publish(channel) {

        if (!channels[channel]) {
            return false;
        }
        var args = Array.prototype.slice.call(arguments, 1);
        for (var i = 0, l = channels[channel].length; i < l; i += 1) {
            var subscription = channels[channel][i];
            if (typeof subscription.context === undefined) {
                subscription.callback(args);
            } else {
                subscription.callback.call(subscription.context, args);
            }
        }
    }

    return {
        subscribe: subscribe,
        publish: publish
    }

})();
console.log("...");


var BackendConnector = (function () {
    function send() {
        $.ajax({
            // url: "http://fettblog.eu/fh/numbers.php?jsonp",
            url: "http://dragonquest.at/numbers.php?jsonp",
            dataType: 'jsonp',
            success: function (data) {
                Mediator.publish('datareceived',data);
            },
            error: function (err) {

            }
        });
    }

    Mediator.subscribe('inputGiven', send);

})();

$('html').on('mousedown keydown tap', function () {
    
    // a new run is only possible, if the slotmachine is not running currently
    if ( !$('#slot1').hasClass('activatedSlot1')
      && !$('#slot2').hasClass('activatedSlot2')
      && !$('#slot3').hasClass('activatedSlot3') ) {
        
        Mediator.publish('inputGiven');
    }
});

Mediator.subscribe('datareceived', datareceived);

function datareceived (data) {
    var dataObj = data[0];
    
    var slot1Id = dataObj.slots[0];
    var slot2Id = dataObj.slots[1];
    var slot3Id = dataObj.slots[2];
    var result  = (+dataObj.result);

    var slotsObj = $('.slots');

    console.log("===========================> " + slot1Id + " " + slot2Id + " " + slot3Id + " = " + result);

    // register all listeners separately for all three slots,
    // so that the animation-iteration-count in main.scss has an effect
    $('#slot1').on('animationstart', slot1Listener);
    $('#slot1').on('animationend',   slot1Listener);

    $('#slot2').on('animationstart', slot2Listener);
    $('#slot2').on('animationend',   slot2Listener);

    $('#slot3').on('animationstart', slot3Listener);
    $('#slot3').on('animationend',   slot3Listener);
    
    // activate animations ...
    $('#slot1').addClass('activatedSlot1');
    $('#slot2').addClass('activatedSlot2');
    $('#slot3').addClass('activatedSlot3');


    function slot1Listener(e) {
      var slot          = $('#slot1');
      var slotId        = slot1Id;
      var slotListener  = slot1Listener;
      var activatedSlot = 'activatedSlot1';
      
      switch(e.type) {
        
        case 'animationstart':
          $('div.score').text("");
          
          // play start sound and start slot sound loop
          $('#startSound')[0].play();
          $('#slotSound')[0].loop = true;
          $('#slotSound')[0].play();

          slot.removeClass('cherry').removeClass('enemy').removeClass('vegetable').removeClass('star');
          slot.off('animationstart', slotListener); // prevents multiple executions by different registered listeners
          break;
        
        case 'animationend':
          slot.removeClass(activatedSlot);
          setEndState(slot, slotId);
          slot.off('animationend', slotListener);
          break;   
      }
    }

    function slot2Listener(e) {
      var slot          = $('#slot2');
      var slotId        = slot2Id;
      var slotListener  = slot2Listener;
      var activatedSlot = 'activatedSlot2';
      
      switch(e.type) {
        
        case 'animationstart':
          slot.removeClass('cherry').removeClass('enemy').removeClass('vegetable').removeClass('star');
          slot.off('animationstart', slotListener); // prevents multiple executions by different registered listeners
          break;
        
        case 'animationend':
          slot.removeClass(activatedSlot);
          setEndState(slot, slotId);
          slot.off('animationend', slotListener);
          break;   
      }
    }

    function slot3Listener(e) {
      var slot          = $('#slot3');
      var slotId        = slot3Id;
      var slotListener  = slot3Listener;
      var activatedSlot = 'activatedSlot3';
      
      switch(e.type) {
        
        case 'animationstart':
          slot.removeClass('cherry').removeClass('enemy').removeClass('vegetable').removeClass('star');
          slot.off('animationstart', slotListener); // prevents multiple executions by different registered listeners
          break;
        
        case 'animationend':
          slot.removeClass(activatedSlot);
          setEndState(slot, slotId);
          evaluateResult(); // assume, slot3 is the last to finish and therefore let it evaluate the final result
          slot.off('animationend', slotListener);
          break;   
      }
    }

    //
    // Stops slot sound, plays win or lose sound and adapts the coins and life count.
    //
    function evaluateResult() {
      
      // stop slot sound, because the animations have ended
      $('#slotSound')[0].loop = false;

      var oldCoins = (+$('div.coin').text());
      var score    = result - 2;
      var newCoins = oldCoins + score;
      var life     = +$('div.life').text();
      
      // debug output
      console.log("Old Coins : " + oldCoins);
      console.log("Result    : " + result);
      console.log("Score     : " + score);
      console.log("New Coins : " + newCoins);
      console.log("Life      : " + life);

      if (score >= 0) {
        $('#winSound')[0].play();
      } else {
        $('#loseSound')[0].play();
      }

      if (newCoins < 0) {
        // coin count is negative
        var newLife = life - 1;

        if (newLife < 0) {
          // no lifes left -> Game Over
          alert("Game Over! :(");
          $('div.coin').text(10);
          $('div.life').text(1);
        } else {
          // lifes left -> reduce life by 1 and set new coin count
          $('div.coin').text(newCoins + 10);
          $('div.life').text(newLife);
        }  
      } else if (newCoins >= 20) {
          // one extra life
          $('div.coin').text(newCoins % 20);
          $('div.life').text(life + 1);
      } else {
        // new coin count is within 0 and 19
        $('div.coin').text(newCoins);
        $('div.score').text("(" + score + ")");
      }
    }

    function setEndState(slotObj, slotId) {
      switch(slotId) {
        case 0:
          slotObj.addClass('cherry');
          break;
        case 1:
          slotObj.addClass('enemy');
          break;
        case 2:
          slotObj.addClass('vegetable');
          break;
        case 3:
          slotObj.addClass('star');
          break;
      }
    }
}